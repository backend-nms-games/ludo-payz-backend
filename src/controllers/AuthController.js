const bcrypt = require('bcrypt');
const check = require('../validation/CheckValidation') 
const conn = require('../../config/db')
 
// Admin login
const authLogin = async (req, res) => {
    let message = null
    let statusCode = 400
    let error = {}
    let data = null
  
    try {
        const errors = check.resultsValidator(req)
        if (errors.length > 0) {
            return res.status(400).json({
                method: req.method,
                status: res.statusCode,
                error: errors
            })
        } else {
            const formData = { 
                email: req.body.email,
                password: req.body.password
            };

            // Check requeted user is exist or not
            let sql = `SELECT * FROM users WHERE LOWER(email)= ? limit ?`;
            let user = await conn.query(sql, [formData.email.toLowerCase(), 1]);
            if (user.length > 0) {
                console.log('user', user)
                const usersRows = (JSON.parse(JSON.stringify(user))[0]);
                const comparison = await bcrypt.compare(formData.password, usersRows.password)
                if (comparison) { 
                    statusCode = 200
                    message = 'Login success'
                    data = {
                        user_id:usersRows.user_id
                    }
                } else {
                    statusCode = 401
                    message = 'Login failed'
                    error.password = "Password does not match!"
                }
            } else {
                statusCode = 404
                message = 'Login failed'
                error.error = "Username does not exist!"
            }
            const responseData = {
                status: statusCode,
                message,
                user: data,
                errors: error
            }
            res.send(responseData)
        }
    } catch (error) {
        res.send({ authLogin: error })
    }
}
 
module.exports = {
    authLogin 
}